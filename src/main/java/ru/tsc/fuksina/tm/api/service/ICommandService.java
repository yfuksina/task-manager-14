package ru.tsc.fuksina.tm.api.service;

import ru.tsc.fuksina.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
